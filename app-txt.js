var fs = require('fs');
var express = require("express");
var app = express();
var router = express.Router();
var path = __dirname + '/views/';
var request = require('request');
var rp = require('request-promise');
var ejs = require('ejs');
var parseXml = require('xml2js').parseString;
var papa = require('papaparse');

var data = papa.parse(fs.readFileSync('data/livre-des-listes-et-candidats.txt', 'latin1'));
console.log(data);

/*
[ 'Code du département',
  'Libellé du département',
  'Code commune',
  'Libellé commune',
  'N° Panneau Liste',
  'Libellé abrégé liste',
  'Libellé Etendu Liste',
  'Nuance Liste',
  'N° candidat',
  'Sexe candidat',
  'Nom candidat',
  'Prénom candidat',
  'Nationalité',
  'Candidat au conseil communautaire',
  '' ]
*/

var total = 0;
var totalM = 0;
var totalF = 0;
var totalNG = 0;
var totalFR = 0;
var totalEU = 0;
var bdd_nuance = {};
var bdd_listes = {};
var bdd = {};
data.data.forEach(function(d, i){
    if(i<3){
        console.log(d);
    }else{
        if(i==4){
            console.log(d[12]);
        }
        if(d[9]=='F'){
            totalF++
        }else if(d[9]=='M'){
            totalM++;
        }else{
            totalNG++;
        }
        if(d[12]=="Française"){
            totalFR++;
        }else if(d[12] && d[12].length>1){
            //console.log(d[12]);
            totalEU++;
        }
        total++;
        if(d[7] && d[7][0]=='L' && d[7].length<=5){
            if(bdd_nuance[d[7]]==undefined){
                bdd_nuance[d[7]]=1;
            }else{
                bdd_nuance[d[7]]++;
            }
        }
        if(bdd[d[0]+" "+d[2]]==undefined){
            bdd[d[0]+" "+d[2]]={};
        }
        if(bdd[d[0]+" "+d[2]][d[4]]==undefined){
            bdd[d[0]+" "+d[2]][d[4]]=[];
        }
        bdd[d[0]+" "+d[2]][d[4]].push(d[10]+" "+d[11]); 
        
        if(bdd_listes[d[0]+" "+d[2]+" "+d[4]]==undefined){
            if(d[7] && d[7][0]=='L' && d[7].length<=5){
                bdd_listes[d[0]+" "+d[2]+" "+d[4]]=d[7];
            }
        }
    }
});

console.log(total);

console.log("F: "+totalF+" soit "+(totalF/total*100).toFixed(2)+" %");
console.log("M: "+totalM+" soit "+(totalM/total*100).toFixed(2)+" %");
console.log("NG: "+totalNG+" soit "+(totalNG/total*100).toFixed(2)+" %");

console.log("EU: "+(totalEU/total*100).toFixed(2)+" %");

console.log(bdd_nuance);

var bdd_listes_nuance = {};
var totalListes = 0;
for(var liste in bdd_listes){
    var d = bdd_listes[liste];
    totalListes++;
    if(bdd_listes_nuance[d]==undefined){
       bdd_listes_nuance[d]=1; 
    }else{
       bdd_listes_nuance[d]++; 
    }
}

console.log(bdd_listes_nuance);
console.log(totalListes);


/*
var codeIndex = "EssaiMUNICIPALES2020";

app.set('view engine', 'ejs');
app.get('/', function (req, res) {
    res.render('pages/index');
});

app.use('/static', express.static(__dirname + '/static'));
app.use('/faicons', express.static(__dirname + '/dist/Fork-Awesome'));
app.use('/bootstrapcss', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/bootstrapicons', express.static(__dirname + '/node_modules/bootstrap-icons/icons'));
app.use('/bootstrapjs', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/jqueryjs', express.static(__dirname + '/node_modules/jquery/dist'));

app.listen(3202, function () {
    console.log("Live at Port 3202");
});


// api.gouv.fr
app.get('/api.gouv.fr/:q', function (req, res) {
    rp("https://geo.api.gouv.fr/communes?nom=" + req.params.q + "&fields=nom,code,codesPostaux,codeDepartement,codeRegion,population&format=json&geometry=centre")
        .then(function (resultats) {
            res.send(resultats);
        })
        .catch(function (err) {});
});

//communes
app.get('/:dep/:codeInsee/info.json', function (req, res) {
    rp("https://bdd.listesmunicipales.fr/" + req.params.dep + "/" + req.params.codeInsee + "/info.json")
        .then(function (json) {
            json = JSON.parse(json);
            res.send(json);
        })
        .catch(function (err) {
            res.send("404");
        });
});
app.get('/:dep/:codeInsee/listes.json', function (req, res) {
    var depNorma = String(Math.pow(10, (3 - String(req.params.dep).length)) + req.params.dep).slice(1, 4);
    var codeInseeNorma = String(Math.pow(10, (6 - String(req.params.codeInsee).length)) + req.params.codeInsee).slice(1, 7);
    rp({
            url: "https://www.interieur.gouv.fr/avotreservice/elections/telechargements/" + codeIndex + "/resultatsT1/" + depNorma + "/" + codeInseeNorma + ".xml"
        })
        .then(function (xml) {
            parseXml(xml, function (err, result) {
                //res.send(result);
                res.send(result["Election"]["Departement"][0]["Commune"][0]["Tours"][0]["Tour"][0]["Listes"][0]["Liste"]);
            });
        })
        .catch(function (err) {
            res.send("404");
        });
});

function villeAvecArrondissements(dep, codeInsee, req, res) {
    var communeInfo = {};
    var depNorma = String(Math.pow(10, (3 - String(dep).length)) + dep).slice(1, 4);
    var arrondissementNorma = String(Math.pow(10, (2 - String(req.params.arrondissement).length)) + req.params.arrondissement).slice(1, 3);
    var codeInseeNorma = String(Math.pow(10, (6 - String(codeInsee).length)) + codeInsee).slice(1, 7);
    rp({
            url: "https://www.interieur.gouv.fr/avotreservice/elections/telechargements/" + codeIndex + "/candidatureT1/" + depNorma + "/C1" + codeInseeNorma + "SR" + arrondissementNorma + ".xml"
    })
        .then(function (interieurXml) {
            rp("https://bdd.listesmunicipales.fr/" + dep + "/" + codeInsee + "/info.json")
                .then(function (gouvJson) {
                    communeInfo = JSON.parse(gouvJson);
                    var chemin = ["France", dep, communeInfo.nom, req.params.arrondissement];
                    communeInfo.nom += ' ' + req.params.arrondissement;
                    parseXml(interieurXml, function (err, result) {
                        res.render('pages/commune', {
                            chemin: chemin,
                            commune: communeInfo,
                            listes: result["Election"]["Departement"][0]["Commune"][0]["Tours"][0]["Tour"][0]["Listes"][0]["Liste"],
                            nuances: nuances
                        });
                    });
                })
                .catch(function (err) {});
        })
        .catch(function (err) {
            res.send("404");
        });
}
app.get('/75/75056/:arrondissement', function (req, res) {
    villeAvecArrondissements("75", "75056", req, res);
});
app.get('/69/69123/:arrondissement', function (req, res) {
    villeAvecArrondissements("69", "69123", req, res);
});
app.get('/13/13055/:arrondissement', function (req, res) {
    villeAvecArrondissements("13", "13055", req, res);
});
app.get('/:dep/:codeInsee', function (req, res) {
    if (req.params.dep == "75" && req.params.codeInsee == "75056") {
        rp("https://bdd.listesmunicipales.fr/" + req.params.dep + "/" + req.params.codeInsee + "/info.json")
            .then(function (gouvJson) {
                communeInfo = JSON.parse(gouvJson);
                res.render('pages/arrondissements', {
                    chemin: ["France", req.params.dep, communeInfo.nom],
                    commune: communeInfo,
                    arrondissements: 20
                });
            })
            .catch(function (err) {});
    } else if (req.params.dep == "69" && req.params.codeInsee == "69123") {
        rp("https://bdd.listesmunicipales.fr/" + req.params.dep + "/" + req.params.codeInsee + "/info.json")
            .then(function (gouvJson) {
                communeInfo = JSON.parse(gouvJson);
                res.render('pages/arrondissements', {
                    chemin: ["France", req.params.dep, communeInfo.nom],
                    commune: communeInfo,
                    arrondissements: 9
                });
            })
            .catch(function (err) {});
    } else if (req.params.dep == "13" && req.params.codeInsee == "13055") {
        rp("https://bdd.listesmunicipales.fr/" + req.params.dep + "/" + req.params.codeInsee + "/info.json")
            .then(function (gouvJson) {
                communeInfo = JSON.parse(gouvJson);
                res.render('pages/arrondissements', {
                    chemin: ["France", req.params.dep, communeInfo.nom],
                    commune: communeInfo,
                    arrondissements: 8
                });
            })
            .catch(function (err) {
            res.send("404");
        });
    } else {
        var communeInfo = {};
        var depNorma = String(Math.pow(10, (3 - String(req.params.dep).length)) + req.params.dep).slice(1, 4);
        var codeInseeNorma = String(Math.pow(10, (6 - String(req.params.codeInsee).length)) + req.params.codeInsee).slice(1, 7);
        rp({
                url: "https://www.interieur.gouv.fr/avotreservice/elections/telechargements/" + codeIndex + "/candidatureT1/" + depNorma + "/C1" + codeInseeNorma + ".xml"
            })
            .then(function (interieurXml) {
                rp("https://bdd.listesmunicipales.fr/" + req.params.dep + "/" + req.params.codeInsee + "/info.json")
                    .then(function (gouvJson) {
                        communeInfo = JSON.parse(gouvJson);
                        parseXml(interieurXml, function (err, result) {
                            console.log(result["Election"]["Departement"][0]["Commune"][0]["Listes"][0]["Liste"]);
                            res.render('pages/commune', {
                                chemin: ["France", req.params.dep, communeInfo.nom],
                                commune: communeInfo,
                                listes: result["Election"]["Departement"][0]["Commune"][0]["Listes"][0]["Liste"],
                                nuances: nuances
                            });
                        });
                    })
                    .catch(function (err) {
                    res.send("404");
                });
            })
            .catch(function (err) {
            res.send("404");
        });
    }
});

//départements
app.get('/:dep.json', function (req, res) {
    rp("https://bdd.listesmunicipales.fr/index.json")
        .then(function (json) {
            json = JSON.parse(json);
            res.send(json[req.params.dep]);
        })
        .catch(function (err) {
        res.send("404");
    });
});
app.get('/:dep', function (req, res) {
    rp("https://bdd.listesmunicipales.fr/index.json")
        .then(function (json) {
            json = JSON.parse(json);
            //res.send(json[req.params.dep]);
            res.send(req.params.dep);
        })
        .catch(function (err) {
        res.send("404");
    });
});


//nuances
/*var nuances = {
    LEXG: ['Liste Extrême gauche'],
    LFG: ['Liste Front de Gauche'],
    LPG: ['Liste du Parti de Gauche'],
    LCOM: ['Liste du Parti communiste français'],
    LSOC: ['Liste Socialiste'],
    LUG: ['Liste Union de la Gauche'],
    LDVG: ['Liste Divers gauche'],
    LVEC: ['Liste Europe-Ecologie-Les Verts'],
    LDIV: ['Liste Divers'],
    LMDM: ['Liste Modem'],
    LUC: ['Liste Union du Centre'],
    LUDI: ['Liste Union Démocrates et Indépendants'],
    LUMP: ['Liste  Union pour un Mouvement Populaire'],
    LUD: ['Liste Union de la Droite'],
    LDVD: ['Liste Divers droite'],
    LFN: ['Liste Front National'],
    LEXD: ['Liste Extrême droite']
};

var nuances = {};
rp({
        url: "https://www.interieur.gouv.fr/avotreservice/elections/telechargements/" + codeIndex + "/referenceMN/nuances.xml",
    })
    .then(function (interieurXml) {
        parseXml(interieurXml, function (err, result) {
            result["Election"]["Nuances"][0]["Nuance"].forEach(function (nuance, i) {
                nuances[nuance["CodNua"]] = nuance["LibNua"];
            });
            console.log(nuances);
        });
    })
    .catch(function (err) {});
    
    */
    