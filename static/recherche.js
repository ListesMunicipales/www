$("#recherche").keyup(chercher);
$("#recherche").parent().click(function(){
    $("#recherche").keyup();
});

function chercher() {
    var query = $(this).val();
    if (query.length >= 3) {
        $.ajax({
            method: "GET",
            url: "api.gouv.fr/" + query
        }).done(function (resultats) {
            //{"nom":"Angers","code":"49007","codesPostaux":["49000","49100"],"codeDepartement":"49","codeRegion":"52","population":151229,"_score":1}
            resultats=JSON.parse(resultats);
            console.log(resultats);
            $("#resultats").html("<p>"+resultats.length+" résulat"+[resultats.length>1 ? "s" : ""]+"</p>");
            resultats.forEach(function(commune){
                var codesPostaux="";
                commune.codesPostaux.forEach(function(cp){
                   codesPostaux+='<span class="badge badge-pill badge-light" title="Code postal '+cp+'">'+cp+'</span> '; 
                });
                $("#resultats").append('<a href="/'+commune.codeDepartement+'/'+commune.code+'" class="card"><div class="card-body"><h5 class="card-title">'+commune.nom+'</h5><p class="card-text">'+codesPostaux+'</p></div></a>');
            });
        });
    }else{
        $("#resultats").html('<p class="text-muted text-center pt-2"><i class="fa fa-share fa-rotate-270" aria-hidden="true"></i> Commencez par écrire le nom d\'une commune dans la barre de recherche ci-dessus.</p>');
    }
}